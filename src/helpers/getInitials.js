export function getInitials(value) {
  if (!value) {
    return '';
  }

  return value
    .match(/(\b)(\w)/g)
    .slice(0, 2)
    .join('')
    .toUpperCase();
}
