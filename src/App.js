import React from 'react';
import styles from './App.module.scss';
import ManagerSearch from './ManagerSearch/containers/ManagerSearch.container';

function App() {
  return (
    <div className={styles.container}>
      <ManagerSearch />
    </div>
  );
}

export default App;
