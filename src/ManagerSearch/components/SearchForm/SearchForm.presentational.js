import React from 'react';
import cx from 'classnames';
import styles from './searchForm.module.scss';

const Keys = {
  ARROW_UP: 38,
  ARROW_DOWN: 40,
  ENTER: 13,
  ESC: 27,
};

const noop = () => {};

function SearchForm({
  onSearch,
  searchValue,
  isOpen,
  onArrowDownPressed = noop,
  onArrowUpPressed = noop,
  onEnterPressed = noop,
  onFormClick = noop,
}) {
  function onSearchChange(e) {
    const { value } = e.target;
    onSearch(value);
  }

  function onKeyDown(e) {
    switch (e.keyCode) {
      case Keys.ARROW_DOWN:
        e.preventDefault();
        onArrowDownPressed();
        break;
      case Keys.ARROW_UP:
        e.preventDefault();
        onArrowUpPressed();
        break;
      case Keys.ENTER:
        e.preventDefault();
        onEnterPressed();
        break;
      default:
    }
  }

  function onClick(e) {
    e.stopPropagation();
    onFormClick();
  }

  return (
    <div className={styles.searchForm} onKeyDown={onKeyDown} onClick={onClick}>
      <div className={styles.inputboxContainer}>
        <input
          type="text"
          onChange={onSearchChange}
          value={searchValue}
          placeholder="Choose Manager"
          className={styles.inputbox}
          autoFocus
        />
        <span
          role="button"
          tabIndex="0"
          className={cx(styles.caret, {
            [styles.open]: isOpen,
          })}
        ></span>
      </div>
    </div>
  );
}

export default SearchForm;
