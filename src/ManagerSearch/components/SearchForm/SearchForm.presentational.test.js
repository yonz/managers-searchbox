import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import SearchForm from './SearchForm.presentational';

describe('SearchForm', () => {
  const value = 'le value';

  describe('setting a value by default', () => {
    it('should set the value of the input', () => {
      const { getByRole } = render(<SearchForm searchValue={value} />);
      expect(getByRole('textbox').value).toBe(value);
    });
  });

  describe('clicking on the form', () => {
    it('should call onFormClick', () => {
      const onFormClick = jest.fn();
      const { getByRole } = render(<SearchForm onFormClick={onFormClick} />);

      fireEvent.click(getByRole('textbox'));

      expect(onFormClick).toHaveBeenCalled();
    });
  });

  describe('changing the value of the input', () => {
    it('should call onSearch', () => {
      const onSearch = jest.fn();
      const { getByRole } = render(<SearchForm onSearch={onSearch} />);

      fireEvent.change(getByRole('textbox'), { target: { value } });

      expect(onSearch).toHaveBeenCalledWith(value);
    });
  });

  describe('when pressing', () => {
    describe('the arrow up key', () => {
      it('should call onArrowUpPressed', () => {
        const onArrowUpPressed = jest.fn();
        const { getByRole } = render(
          <SearchForm onArrowUpPressed={onArrowUpPressed} />
        );
        const textbox = getByRole('textbox');

        fireEvent.click(textbox);
        fireEvent.keyDown(textbox, {
          key: 'ArrowUp',
          keyCode: 38,
        });

        expect(onArrowUpPressed).toHaveBeenCalled();
      });
    });

    describe('the arrow down key', () => {
      it('should call onArrowDownPressed', () => {
        const onArrowDownPressed = jest.fn();
        const { getByRole } = render(
          <SearchForm onArrowDownPressed={onArrowDownPressed} />
        );
        const textbox = getByRole('textbox');

        fireEvent.click(textbox);
        fireEvent.keyDown(getByRole('textbox'), {
          key: 'ArrowDown',
          keyCode: 40,
        });

        expect(onArrowDownPressed).toHaveBeenCalled();
      });
    });

    describe('the ENTER key', () => {
      it('should call onEnterPressed', () => {
        const onEnterPressed = jest.fn();
        const { getByRole } = render(
          <SearchForm onEnterPressed={onEnterPressed} />
        );
        const textbox = getByRole('textbox');

        fireEvent.click(textbox);
        fireEvent.keyDown(getByRole('textbox'), {
          key: 'Enter',
          keyCode: 13,
        });

        expect(onEnterPressed).toHaveBeenCalled();
      });
    });
  });
});
