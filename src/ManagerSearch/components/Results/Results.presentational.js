import React from 'react';
import cx from 'classnames';
import styles from './results.module.scss';
import { getInitials } from '../../../helpers/getInitials';

const colors = [styles.yellow, styles.green, styles.blue, styles.gray];

function* getColorClass() {
  let i = 0;

  while (true) {
    yield colors[i];
    i += 1;
    if (i >= colors.length) {
      i = 0;
    }
  }
}

const colorGenerator = getColorClass();

function Results({ items, selectedIndex, onClick }) {
  return (
    <ul role="listbox" tabIndex="0" className={styles.results}>
      {items.map((item, index) => (
        <li
          key={item.id}
          role="option"
          aria-selected={selectedIndex === index}
          className={cx(styles.resultItem, {
            [styles.selected]: selectedIndex === index,
          })}
          onClick={() => onClick(index)}
        >
          <span className={cx(styles.initials, colorGenerator.next().value)}>
            {getInitials(item.getAttribute('name'))}
          </span>
          <span>
            <span className={styles.name}>{item.getAttribute('name')}</span>
            <span className={styles.email}>{item.getAttribute('email')}</span>
          </span>
        </li>
      ))}
    </ul>
  );
}

export default Results;
