import React, { useEffect, useState } from 'react';
import Fuse from 'fuse.js';
import Employee from '../../models/employee.model';
import SearchForm from '../components/SearchForm/SearchForm.presentational';
import Results from '../components/Results/Results.presentational';

export const fuseOptions = {
  threshold: 0.2,
  location: 0,
  distance: 100,
  keys: ['attributes.data.name', 'attributes.data.email'],
};

function ManagerSearch() {
  const [managers, setManagers] = useState([]);
  const [filteredResults, setFilteredResults] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [searchValue, setSearchValue] = useState('');
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    Employee.get().then(response => {
      const managers = response.data.map(e => {
        const manager = e.manager().getReferringObject();
        manager.setAttribute(
          'email',
          manager.getAccount().getAttribute('email')
        );
        return manager;
      });

      setManagers(managers);
      setFilteredResults(managers);
    });
  }, []);

  function onFormSearch(value) {
    const fuse = new Fuse(managers, fuseOptions);

    setSelectedIndex(0);
    setSearchValue(value);
    setIsOpen(true);

    if (!value) {
      setFilteredResults(managers);
      return;
    }

    setFilteredResults(fuse.search(value));
  }

  function onNavigateDown() {
    const nextIndex = selectedIndex + 1;
    if (nextIndex <= filteredResults.length - 1) {
      setSelectedIndex(nextIndex);
    }
  }

  function onNavigateUp() {
    const nextIndex = selectedIndex - 1;
    if (nextIndex >= 0) {
      setSelectedIndex(nextIndex);
    }
  }

  function onItemSelected(index) {
    index = index !== undefined ? index : selectedIndex;
    setSearchValue(filteredResults[selectedIndex].getAttribute('name'));
    setSelectedIndex(index);
    setIsOpen(false);
  }

  function onFormClick() {
    setIsOpen(!isOpen);
  }

  return (
    <>
      <SearchForm
        searchValue={searchValue}
        onSearch={onFormSearch}
        onArrowUpPressed={onNavigateUp}
        onArrowDownPressed={onNavigateDown}
        onEnterPressed={onItemSelected}
        onFormClick={onFormClick}
        isOpen={isOpen}
      />
      {isOpen && (
        <Results
          items={filteredResults}
          selectedIndex={selectedIndex}
          onClick={onItemSelected}
        />
      )}
    </>
  );
}

export default ManagerSearch;
