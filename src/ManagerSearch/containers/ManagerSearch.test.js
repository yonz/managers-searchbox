import React from 'react';
import {
  render,
  fireEvent,
  getByText as stGetByText,
  act,
} from '@testing-library/react';
import Employee from '../../models/employee.model';
import ManagerSearch from './ManagerSearch.container';
import { managerFactory } from '../../factories/managers';

describe('ManagerSearch', () => {
  beforeEach(() => {
    Employee.get = jest.fn().mockResolvedValue(managerFactory());
  });

  describe('clicking on the inputbox', () => {
    it('should show the list with all the result', async () => {
      let component;
      await act(async () => await (component = render(<ManagerSearch />)));

      const { getByRole, getAllByRole } = component;

      fireEvent.click(component.getByRole('textbox'));

      expect(getByRole('listbox')).toBeTruthy();
      expect(getAllByRole('option')).toHaveLength(3);
    });

    it('should render the corresponding names', async () => {
      let component;
      await act(async () => await (component = render(<ManagerSearch />)));

      fireEvent.click(component.getByRole('textbox'));

      const { getByText } = component;
      expect(getByText('John Doe')).toBeTruthy();
    });

    it('should render the corresponding emails', async () => {
      let component;
      await act(async () => await (component = render(<ManagerSearch />)));

      fireEvent.click(component.getByRole('textbox'));

      const { getByText } = component;
      expect(getByText('john@doe.com')).toBeTruthy();
    });
  });

  describe('changing the value of the inputbox', () => {
    const value = 'john';

    it('should show the list with filtered results', async () => {
      let component;
      await act(async () => await (component = render(<ManagerSearch />)));

      const { getByRole, getAllByRole } = component;

      fireEvent.change(getByRole('textbox'), { target: { value } });

      expect(getByRole('listbox')).toBeTruthy();
      expect(getAllByRole('option')).toHaveLength(2);
    });
  });

  describe('using the arrow keys', () => {
    it('should navigate the list downwards', async () => {
      let component;
      await act(async () => await (component = render(<ManagerSearch />)));

      const { getByRole, container } = component;

      fireEvent.click(component.getByRole('textbox'));

      fireEvent.keyDown(getByRole('textbox'), {
        key: 'ArrowDown',
        keyCode: 40,
      });

      expect(
        stGetByText(
          container.querySelector('[aria-selected="true"]'),
          'Paul Sam'
        )
      ).toBeTruthy();
    });

    it('should navigate the list upwards', async () => {
      let component;
      await act(async () => await (component = render(<ManagerSearch />)));

      const { getByRole, container } = component;

      fireEvent.click(component.getByRole('textbox'));

      fireEvent.keyDown(getByRole('textbox'), {
        key: 'ArrowDown',
        keyCode: 40,
      });

      fireEvent.keyDown(getByRole('textbox'), {
        key: 'ArrowUp',
        keyCode: 38,
      });

      expect(
        stGetByText(
          container.querySelector('[aria-selected="true"]'),
          'John Doe'
        )
      ).toBeTruthy();
    });
  });
});
