export const managerFactory = () => {
  return {
    data: [
      {
        manager: () => {
          return {
            getReferringObject: () => {
              return {
                attributes: {
                  data: {
                    name: 'John Doe',
                  },
                },
                id: '1',
                getAttribute: attr => {
                  switch (attr) {
                    case 'name':
                      return 'John Doe';
                    case 'email':
                      return 'john@doe.com';
                    default:
                  }
                },
                setAttribute: () => {},
                getAccount: () => {
                  return {
                    getAttribute: () => {
                      return 'john@doe.com';
                    },
                  };
                },
              };
            },
          };
        },
      },
      {
        manager: () => {
          return {
            getReferringObject: () => {
              return {
                id: '2',
                attributes: {
                  data: {
                    name: 'Paul Sam',
                  },
                },
                getAttribute: attr => {
                  switch (attr) {
                    case 'name':
                      return 'Paul Sam';
                    case 'email':
                      return 'paul@sam.com';
                    default:
                  }
                },
                setAttribute: () => {},
                getAccount: () => {
                  return {
                    getAttribute: () => {
                      return 'paul@sam.com';
                    },
                  };
                },
              };
            },
          };
        },
      },
      {
        manager: () => {
          return {
            getReferringObject: () => {
              return {
                id: '3',
                attributes: {
                  data: {
                    name: 'Johnatan Stewart',
                  },
                },
                getAttribute: attr => {
                  switch (attr) {
                    case 'name':
                      return 'Johnatan Stewart';
                    case 'email':
                      return 'johnatan@stewart.com';
                    default:
                  }
                },
                setAttribute: () => {},
                getAccount: () => {
                  return {
                    getAttribute: () => {
                      return 'johnatan@stewart.com';
                    },
                  };
                },
              };
            },
          };
        },
      },
    ],
  };
};
