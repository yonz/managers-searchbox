import { Model } from 'coloquent';

class Account extends Model {
  jsonApiType = 'accounts';

  getJsonApiBaseUrl() {
    return '';
  }
}

export default Account;
