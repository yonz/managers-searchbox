import { Model } from 'coloquent';
import Account from './account.model';

export const RESOURCE_URL =
  'https://gist.githubusercontent.com/daviferreira/41238222ac31fe36348544ee1d4a9a5e/raw/5dc996407f6c9a6630bfcec56eee22d4bc54b518';

class Employee extends Model {
  // this is not supposed to be like this
  // but I readlly didn't want to put the JSON file
  // in the project
  jsonApiType = 'employees.json';

  getJsonApiBaseUrl() {
    return RESOURCE_URL;
  }

  manager() {
    return this.hasOne(Employee);
  }

  account() {
    return this.hasOne(Account);
  }

  getAccount() {
    return this.getRelation('account');
  }
}

export default Employee;
